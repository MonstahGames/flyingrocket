#include "GameScene.h"
#include "PlayerObject.h"
#include "AppDelegate.h"
#include "Discord.h"

USING_NS_CC;

Scene* GameScene::createScene() {
	GameScene *ret = new (std::nothrow) GameScene();
	if (ret && ret->init())
	{
		ret->autorelease();
		return ret;
	}
	else
	{
		CC_SAFE_DELETE(ret);
		return nullptr;
	}
}

bool GameScene::init() {

	if (!Scene::initWithPhysics()) {
		return false;
	}

#if (!NDEBUG)
	this->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
#endif


	player = PlayerObject::create("rocket01.png");
	player->getPhysicsBody()->setEnabled(true);

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	auto rocketPosX = origin.x + (visibleSize.width / 2);
	auto rocketPosY = origin.y + (visibleSize.height / 2) - 324;
	player->setPosition(Vec2(rocketPosX, rocketPosY));

	this->addChild(player, 10);

	gameFlow = GameFlow::setup(this);
	this->setupBorders();
	this->setupButtons();
	this->setupUI();

	auto titleLabel = Label::createWithTTF("fr 26/10", "fonts/arial.ttf", 20);
	titleLabel->setPosition(origin.x + (visibleSize.width / 2), (visibleSize.height - titleLabel->getContentSize().height) + origin.y);
	this->addChild(titleLabel, 1000000);

	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(GameScene::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	auto keyListener = EventListenerKeyboard::create();
	keyListener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyListener, this);
	Discord::getInstance()->setDetails("In-Game");
	Discord::getInstance()->setLargeImage("rocket01-hd");
#endif

	return true;
}

bool GameScene::onContactBegin(cocos2d::PhysicsContact &contact) {
	if (contact.getShapeB()->getGroup() == 5 && contact.getShapeA()->getGroup() == 15) {
		return true;
	}

	if (!contacted) {
		contacted = true;
	}
	else {
		return true;
	}

	this->killPlayer();
	gameFlow->stopMeteorSpawner();

	auto children = gameFlow->getMeteorSpawner()->getChildren();
	for (Node* child : children) {
		if (child->getName() == "meteor") {
			child->stopAllActions();

			auto rot = RotateBy::create(2.25f, Vec3(90, 0, 90));
			child->runAction(rot);

			auto scaleDown = ScaleTo::create(1.75f, 0);
			child->runAction(scaleDown);

			auto visibleSize = Director::getInstance()->getVisibleSize();
			auto moveBy = MoveBy::create(3, Vec2(0, -visibleSize.height - visibleSize.height / 3));

			child->runAction(moveBy);
		}
	}

	return true;
}

void GameScene::setupBorders() {
	auto borderOne = Border::create(false);
	this->addChild(borderOne, 25);
	
	auto borderTwo = Border::create(true);
	this->addChild(borderTwo, 25);
}

void GameScene::killPlayer() {
	log("Player has hit an object, killing...");
	this->isKilled = true;
	gameFlow->getScoreManager()->end();
	gameFlow->stopScoring();
	this->removeChild(buttonLeft, true);
	this->removeChild(buttonRight, true);
	this->removeChild(menu, true);
	this->removeChild(player, true);
	log("Player has been killed");

	this->getActionManager()->pauseAllRunningActions();

	auto deathLayer = DeathLayer::initLayer();
	this->addChild(deathLayer);
}

void GameScene::goLeft(Ref* pSender) {
	player->movePlayer(false, isKilled);
}

void GameScene::goRight(Ref* pSender) {
	player->movePlayer(true, isKilled);
}

void GameScene::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
	if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		this->pauseGame(NULL);
	}
	if (!paused) {
		if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW) {
			this->goLeft(this);
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW) {
			this->goRight(this);
		}
	}
}

void GameScene::pauseGame(Ref* pSender) {
	if (isKilled) {
		return;
	}

	nodes = this->getScheduler()->pauseAllTargets();
	if (paused) {
		this->getScheduler()->resumeTargets(nodes);
		paused = false;
		return;
	}
	paused = true;
}

void GameScene::setupButtons() {
	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	#pragma region Btn1
	buttonLeft = MenuItemImage::create("trs.png", "trs.png", CC_CALLBACK_1(GameScene::goLeft, this));
	buttonLeft->setContentSize(Size(visibleSize.width / 2, visibleSize.height));
	buttonLeft->setPosition(origin.x + (visibleSize.width / 4), (visibleSize.height / 2) + origin.y);
	#pragma endregion	

	#pragma region Btn2
	buttonRight = MenuItemImage::create("trs.png", "trs.png", CC_CALLBACK_1(GameScene::goRight, this));
	buttonRight->setContentSize(Size(visibleSize.width / 2, visibleSize.height));

	buttonRight->setPosition(origin.x + ((visibleSize.width / 2) + (visibleSize.width / 4)), (visibleSize.height / 2) + origin.y);
	#pragma endregion

	menu = Menu::create(buttonLeft, buttonRight, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 100);
}

void GameScene::setupUI() {
	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	auto bgImage1 = Sprite::createWithSpriteFrameName("bg01.png");
	auto bgImage2 = Sprite::createWithSpriteFrameName("bg02.png");

	auto bgWidth = bgImage1->getContentSize().width;
	auto bgHeight = bgImage1->getContentSize().height;
	auto vWidth = visibleSize.width;
	auto vHeight = visibleSize.height;

	bgImage1->setScale(vWidth / bgWidth, vHeight / bgHeight);
	bgImage1->setPosition(origin.x + (vWidth / 2), (vHeight / 2) + origin.y);

	bgImage2->setScale(vWidth / bgWidth, vHeight / bgHeight);
	bgImage2->setPosition(origin.x + (vWidth / 2), (vHeight / 2) + origin.y);

	this->addChild(bgImage1, -999);
	this->addChild(bgImage2, -1000);

	auto fadeOut = FadeOut::create(12.5f);
	auto fadeIn = FadeIn::create(12.5f);

	auto seq = Sequence::create(fadeOut, fadeIn, nullptr);
	bgImage1->runAction(seq);
}
#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "PlayerObject.h"
#include "Border.h"
#include "DeathLayer.h"
#include "GameFlow.h"

class GameScene : public cocos2d::Scene {

public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	bool onContactBegin(cocos2d::PhysicsContact& contact);
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

	void setupBorders();
	void setupButtons();
	void setupUI();

	void goLeft(cocos2d::Ref* pSender);
	void goRight(cocos2d::Ref* pSender);

	void pauseGame(cocos2d::Ref* pSender);
	void killPlayer();

	PlayerObject* player;

	CREATE_FUNC(GameScene);

private:
	bool contacted = false;
	bool paused = false;
	bool isKilled = false;

	std::set<void*> nodes;

	cocos2d::Menu* menu;
	cocos2d::MenuItemImage* buttonLeft;
	cocos2d::MenuItemImage* buttonRight;

	GameFlow* gameFlow;
};

#endif //__GAME_SCENE_H__
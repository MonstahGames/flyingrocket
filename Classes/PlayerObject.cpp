#include "PlayerObject.h"

USING_NS_CC;

PlayerObject::PlayerObject () {}

PlayerObject::~PlayerObject() {}

PlayerObject* PlayerObject::dObject = nullptr;

PlayerObject* PlayerObject::getInstance() {
	if (!dObject) {
		dObject = new PlayerObject;
	}

	return dObject;
}

PlayerObject* PlayerObject::create(const std::string& file) {
	PlayerObject* pObj = new PlayerObject();

	if (pObj->initWithSpriteFrameName(file)) {
		pObj->autorelease();
		pObj->initSprite();

		return pObj;
	}

	CC_SAFE_DELETE(pObj);
	return NULL;
}

void PlayerObject::initSprite() {
	this->setScale(0.25f);
	this->dObject = this;

	auto physicsBody = PhysicsBody::createBox(Size(180, 225), PhysicsMaterial(0.1f, 1.0f, 0.0f));
	physicsBody->setDynamic(false);
	physicsBody->setCollisionBitmask(2);
	physicsBody->setContactTestBitmask(true);
	physicsBody->setGroup(10);
	this->setPhysicsBody(physicsBody);
	this->setName("player");

	log("Player: Init");
}

void PlayerObject::movePlayer(bool right, bool isDead) {

	auto visibleSize = Director::getInstance()->getVisibleSize();
	float adder = visibleSize.width / 6;

	if (isDead) {
		return;
	}

	if (right) {
		auto moveBy = MoveBy::create(globalSpeed, Vec2(adder, 0));
		this->runAction(moveBy);
	}
	else {
		auto moveBy = MoveBy::create(globalSpeed, Vec2(-adder, 0));
		this->runAction(moveBy);
	}
}

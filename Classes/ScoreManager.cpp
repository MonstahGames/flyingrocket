#include "ScoreManager.h"

USING_NS_CC;

ScoreManager* ScoreManager::start() {
	ScoreManager* scoreManager = new ScoreManager();
	scoreManager->initManager();

	return scoreManager;
}

void ScoreManager::initManager() {

	log("Score: Init");

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	hudBg = Sprite::createWithSpriteFrameName("hud.png");
	hudBg->setScale(0.50f);
	hudBg->setPosition(origin.x + (visibleSize.width - 65), (visibleSize.height - 75) + origin.y);
	this->addChild(hudBg, 24);

	scoreLabel = Label::createWithTTF("0", "fonts/arial.ttf", 47);
	scoreLabel->setColor(Color3B::BLACK);
	scoreLabel->setPosition(hudBg->getPositionX() + 15, hudBg->getPositionY());

	this->addChild(scoreLabel, 25);
}

void ScoreManager::update(float delta) {
	currentScore++;
	std::string str = std::to_string(currentScore);
	scoreLabel->setString(str);
}

int ScoreManager::end() {
	this->unscheduleUpdate();
	this->removeChild(hudBg, true);
	this->removeChild(scoreLabel, true);

	int highScore = this->getHighScore();
	if (currentScore > highScore) {
		this->setHighScore(currentScore);
	}

	return currentScore;
}

int ScoreManager::getHighScore() {
	int score = UserDefault::getInstance()->getIntegerForKey("fr_highScore", 0);
	return score;
}

void ScoreManager::setHighScore(int highScore) {
	UserDefault::getInstance()->setIntegerForKey("fr_highScore", highScore);
	UserDefault::getInstance()->flush();
}
#ifndef __DEATH_LAYER_H__
#define __DEATH_LAYER_H__

#include "cocos2d.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "ScoreManager.h"

class DeathLayer : public cocos2d::Layer {
public:

	static cocos2d::Layer* initLayer();

	virtual bool init();

	CREATE_FUNC(DeathLayer);

private:

	void restart(cocos2d::Ref* pSender);
	void exit(Ref* pSender);
};

#endif //__DEATH_LAYER_H__

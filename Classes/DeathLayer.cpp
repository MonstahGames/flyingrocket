#include "DeathLayer.h"

USING_NS_CC;

Layer* DeathLayer::initLayer()
{
	DeathLayer *ret = new (std::nothrow) DeathLayer();
	if (ret && ret->init())
	{
		ret->autorelease();
		return ret;
	}
	else
	{
		CC_SAFE_DELETE(ret);
		return nullptr;
	}
}

bool DeathLayer::init() {

	if (!Layer::init()) {
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	auto restartText = Label::createWithTTF("Restart", "fonts/arial.ttf", 30);
	restartText->setPosition(origin.x + (visibleSize.width / 2), ((visibleSize.height / 2) + 50) + origin.y);

	this->addChild(restartText, 50);

	auto highScoreTitle = Label::createWithTTF("High Score", "fonts/arial.ttf", 40);
	highScoreTitle->setPosition(origin.x + (visibleSize.width / 2), ((visibleSize.height / 2) + 325) + origin.y);

	this->addChild(highScoreTitle, 50);
	
	auto highScoreText = Label::createWithTTF(std::to_string(ScoreManager::getHighScore()), "fonts/arial.ttf", 45);
	highScoreText->setPosition(origin.x + (visibleSize.width / 2), ((visibleSize.height / 2) + 250) + origin.y);

	this->addChild(highScoreText, 50);

	auto restartBtn = MenuItemImage::create("CloseNormal.png", "CloseNormal.png", CC_CALLBACK_1(DeathLayer::restart, this));
	restartBtn->setPosition(origin.x + (visibleSize.width / 2), (visibleSize.height / 2) + origin.y);

	auto exitText = Label::createWithTTF("Exit", "fonts/arial.ttf", 30);
	exitText->setPosition(origin.x + (visibleSize.width / 2), ((visibleSize.height / 2) + 150) + origin.y);

	this->addChild(exitText, 50);

	auto exitBtn = MenuItemImage::create("CloseNormal.png", "CloseNormal.png", CC_CALLBACK_1(DeathLayer::exit, this));
	exitBtn->setPosition(origin.x + (visibleSize.width / 2), ((visibleSize.height / 2) + 100) + origin.y);
	
	auto menu = Menu::create(restartBtn, exitBtn, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);

	return true;
}

void DeathLayer::restart(Ref* pSender) {
	auto gameScene = GameScene::createScene();
	auto fade = TransitionFade::create(0.15f, gameScene);
	Director::getInstance()->replaceScene(fade);
}

void DeathLayer::exit(Ref* pSender) {
	auto menuScene = MenuScene::createScene();
	auto fade = TransitionFade::create(0.25f, menuScene);
	Director::getInstance()->replaceScene(fade);
}

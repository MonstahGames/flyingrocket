#ifndef __DISCORD_H__
#define __DISCORD_H__

#include "cocos2d.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "discord/discord.h"
#endif

class Discord : public cocos2d::Node {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
public:

	static Discord* getInstance();

	void Initialize();

	void updateActivity(discord::Activity activity);

	void runCallbacks();

	void setState(char* state);
	void setDetails(char* details);
	void setLargeImage(char* image);
#endif
private:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	static Discord* dObject;
	const std::int64_t clientId = 386528708347035648;

	discord::Core* core;
	discord::Activity currentActivity;
#endif
};

#endif //__DISCORD_H__

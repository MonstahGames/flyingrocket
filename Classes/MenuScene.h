#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__

#include "cocos2d.h"

class MenuScene : public cocos2d::Scene
{
public:

    static cocos2d::Scene* createScene();

    CREATE_FUNC(MenuScene);

private:

    virtual bool init();

	void update(float delta);

	void playGame(cocos2d::Ref* pSender);
};

#endif // __MENU_SCENE_H__

#include "Border.h"

USING_NS_CC;

Border* Border::create(bool state) {
	Border* pObj = new Border();

	if (pObj->initWithFile("trs.png")) {
		pObj->autorelease();
		pObj->initBorder(state);

		return pObj;
	}

	CC_SAFE_DELETE(pObj);
	return NULL;
}

bool Border::initBorder(bool state) {

	if (!Sprite::initWithFile("trs.png")) {
		return false;
	}

	auto director = Director::getInstance();
	auto runningScene = director->getRunningScene();
	auto visibleSize = director->getVisibleSize();
	auto origin = director->getVisibleOrigin();

	this->setContentSize(Size(1, visibleSize.height));

	if (state) {
		this->setPosition(origin.x + visibleSize.width, (visibleSize.height / 2) + origin.y);
	}
	else {
		this->setPosition(origin.x, (visibleSize.height / 2) + origin.y);
	}

	auto physicsBody = PhysicsBody::createBox(Size(1, visibleSize.height), PhysicsMaterial(0.1f, 1.0f, 0.0f));
	physicsBody->setDynamic(false);
	physicsBody->setCollisionBitmask(2);
	physicsBody->setContactTestBitmask(true);
	physicsBody->setGroup(5);

	this->setPhysicsBody(physicsBody);

	return true;
}
#ifndef __BORDER_H__
#define __BORDER_H__

#include "cocos2d.h"

class Border : public cocos2d::Sprite {
public:
	static Border* create(bool state);

	bool initBorder(bool state);
};
#endif //__BORDER_H__
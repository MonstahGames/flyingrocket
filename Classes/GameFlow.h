#ifndef __GAME_FLOW_H__
#define __GAME_FLOW_H__

#include "cocos2d.h"
#include "MeteorSpawner.h"
#include "ScoreManager.h"
#include "Discord.h"

class GameFlow : public cocos2d::Node {
public:
	static GameFlow* setup(cocos2d::Scene* scene);

	MeteorSpawner* getMeteorSpawner();

	void stopMeteorSpawner();

	ScoreManager* getScoreManager();
	
	void stopScoring();

private:
	bool init(cocos2d::Scene* scene);
	virtual void update(float delta);

	MeteorSpawner* meteorSpawner;
	bool meteorSpawnerEnabled = true;

	ScoreManager* scoreManager;
	bool scoreManagerEnabled = true;
};
#endif //__GAME_FLOW_H__
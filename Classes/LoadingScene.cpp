#include "LoadingScene.h"
#include "MenuScene.h"
#include "Discord.h"

USING_NS_CC;

Scene* LoadingScene::createScene() {
	return LoadingScene::create();
}

bool LoadingScene::init() {

	if (!Scene::init()) {
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	auto sigmaLogo = Sprite::create("sigmaLogo.png");
	sigmaLogo->setPosition(origin.x + (visibleSize.width / 2), (visibleSize.height / 2 + (sigmaLogo->getContentSize().height / 4)) + origin.y);
	sigmaLogo->setScale(0.75f);

	this->addChild(sigmaLogo, 10);

	log("Starting Texture Load...");
	CallFunc* runCallback = CallFunc::create(CC_CALLBACK_0(LoadingScene::loadGame, this));
	this->runAction(Sequence::create(DelayTime::create(2), runCallback, nullptr));

	return true;
}

void LoadingScene::loadGame() {
	
	SpriteFrameCache* frameCache = SpriteFrameCache::getInstance();

	frameCache->addSpriteFramesWithFile("frRocketSheet.plist");
	frameCache->addSpriteFramesWithFile("frRocketSheet-hd.plist");
	frameCache->addSpriteFramesWithFile("frRocketSheet-hd-new.plist");
	frameCache->addSpriteFramesWithFile("frUISheet.plist");
	frameCache->addSpriteFramesWithFile("frGameSheet.plist");

	log("Finished Texture Load");

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	Discord::getInstance()->Initialize();
#endif
	Director::getInstance()->replaceScene(MenuScene::createScene());
}
#include "Meteor.h"

USING_NS_CC;

Meteor* Meteor::create(const std::string& file, float scale) {

	Meteor* pObj = new Meteor();

	if (pObj->initWithSpriteFrameName(file)) {
		pObj->autorelease();
		pObj->initMeteor(scale);
		pObj->setName("meteor");

		return pObj;
	}

	CC_SAFE_DELETE(pObj);
	return NULL;
}

void Meteor::initMeteor(float scale) {
	this->setScale(scale);
	auto physicsBody = PhysicsBody::createBox(this->getBoundingBox().size, PhysicsMaterial(0.1f, 1.0f, 0.0f));
	physicsBody->setDynamic(false);
	physicsBody->setCollisionBitmask(2);
	physicsBody->setContactTestBitmask(true);
	physicsBody->setGroup(15);
	this->setPhysicsBody(physicsBody);

	this->moveMeteor();
}

void Meteor::moveMeteor() {

	auto origin = Director::getInstance()->getVisibleOrigin();
	float y = Director::getInstance()->getVisibleSize().height;

	auto moveBy = MoveBy::create(1.75f, Vec2(0, -y - y / 3));
	auto sequence = Sequence::create(moveBy, CallFuncN::create(CC_CALLBACK_0(Meteor::destroy, this)), nullptr);
	
	this->runAction(sequence);
}

void Meteor::destroy() {
	this->removeFromParent();
	this->release();
}
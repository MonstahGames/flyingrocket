#include "MenuScene.h"
#include "GameScene.h"
#include "Discord.h"

USING_NS_CC;

Scene* MenuScene::createScene() {
    return MenuScene::create();
}

bool MenuScene::init()
{
    if (!Scene::init()) {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto origin = Director::getInstance()->getVisibleOrigin();

	auto playBtn = MenuItemImage::create();
	playBtn->setCallback(CC_CALLBACK_1(MenuScene::playGame, this));
	playBtn->setSpriteFrameName("playButton.png", "playButtonSelected.png");
	playBtn->setPosition(origin.x + (visibleSize.width / 2), (visibleSize.height / 2) + origin.y);
	playBtn->setScale(0.5f);

    auto menu = Menu::create(playBtn, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    auto titleLabel = Label::createWithTTF("Flying Rocket\nv0.53-alpha", "fonts/arial.ttf", 48);
	titleLabel->setAlignment(TextHAlignment::CENTER);
	titleLabel->setPosition(origin.x + (visibleSize.width / 2), (visibleSize.height - titleLabel->getContentSize().height) + origin.y);
    this->addChild(titleLabel, 1);

	auto authorLabel = Label::createWithTTF("Thybe Van Beersel 2017-2019", "fonts/arial.ttf", 30);
	authorLabel->setPosition(origin.x + (visibleSize.width / 2), 100 + origin.y);
	this->addChild(authorLabel, 1);

	Sprite* bgImage = Sprite::createWithSpriteFrameName("bg02.png");

	auto bgWidth = bgImage->getContentSize().width;
	auto bgHeight = bgImage->getContentSize().height;
	auto vWidth = visibleSize.width;
	auto vHeight = visibleSize.height;

	bgImage->setScale(vWidth / bgWidth, vHeight / bgHeight);
	bgImage->setPosition(origin.x + (vWidth / 2), (vHeight / 2) + origin.y);

	this->addChild(bgImage, -9999);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	auto discord = Discord::getInstance();
	discord->setState("Solo (1/1)");
	discord->setDetails("Currently in Main Menu");
	discord->setLargeImage("bg02");
	this->scheduleUpdate();
#endif

    return true;
}

void MenuScene::update(float delta) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	Discord::getInstance()->runCallbacks();
#endif
}

void MenuScene::playGame(cocos2d::Ref* pSender) {
	log("Game: Start");
	this->unscheduleUpdate();

	auto fade = TransitionFade::create(0.25f, GameScene::createScene());
	Director::getInstance()->replaceScene(fade);
}
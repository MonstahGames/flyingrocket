#ifndef __SCORE_MANAGER_H__
#define __SCORE_MANAGER_H__

#include "cocos2d.h"
#include "MeteorSpawner.h"

class ScoreManager : public cocos2d::Node {
public:

	static ScoreManager* start();
	void initManager();

	int end();
	void update(float delta);
	static int getHighScore();
	int currentScore = 0;

private:
	cocos2d::Label* scoreLabel;
	cocos2d::Sprite* hudBg;

	void setHighScore(int highScore);
};

#endif //__SCORE_MANAGER_H__

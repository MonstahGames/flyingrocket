#include "GameFlow.h"
#include "PlayerObject.h"

USING_NS_CC;

GameFlow* GameFlow::setup(cocos2d::Scene* scene) {
	GameFlow* gameFlow = new GameFlow();

	gameFlow->init(scene);
	gameFlow->scheduleUpdate();
	gameFlow->setPosition(Vec2::ZERO);
	scene->addChild(gameFlow);

	return gameFlow;
}

bool GameFlow::init(Scene* scene) {
	meteorSpawner = MeteorSpawner::setup(scene);

	scoreManager = ScoreManager::start();
	scene->addChild(scoreManager, 1000);

	return true;
}

void GameFlow::update(float delta) {
	if (meteorSpawnerEnabled) {
		meteorSpawner->update(delta);
	}

	if (scoreManagerEnabled) {
		scoreManager->update(delta);
	}

	if (scoreManager->currentScore % 1000 == 0) {
		meteorSpawner->meteorSize = 0.25f;
		auto scaleAnim = ScaleTo::create(1.0f, 0.175f);
		PlayerObject::getInstance()->runAction(scaleAnim);
	}

	if (scoreManager->currentScore % 2000 == 0) {
		meteorSpawner->meteorSize = 0.45f;
		auto scaleAnim = ScaleTo::create(1.0f, 0.25f);
		PlayerObject::getInstance()->runAction(scaleAnim);
	}

	if (scoreManager->currentScore % 375 == 0) {
		meteorSpawner->setMeteorSize(meteorSpawner->meteorSize * 1.125f);
	}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	Discord::getInstance()->runCallbacks();
#endif
}

MeteorSpawner* GameFlow::getMeteorSpawner() {
	return meteorSpawner;
}

ScoreManager* GameFlow::getScoreManager() {
	return scoreManager;
}

void GameFlow::stopMeteorSpawner() {
	meteorSpawnerEnabled = false;
}

void GameFlow::stopScoring() {
	scoreManagerEnabled = false;
}

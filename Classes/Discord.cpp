#include "Discord.h"

USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
Discord* Discord::dObject = nullptr;

Discord* Discord::getInstance() {
	if (!dObject) {
		dObject = new Discord;
	}

	return dObject;
}

void Discord::Initialize()
{
	auto result = discord::Core::Create(clientId, DiscordCreateFlags_Default, &core);
	discord::Activity activity{};
	activity.SetState("N/A");
	activity.SetDetails("N/A");
	activity.GetAssets().SetLargeImage("rocket01-hd");
	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {
	});

	currentActivity = activity;

	this->runCallbacks();
}

void Discord::updateActivity(discord::Activity activity) {
	currentActivity = activity;
	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
}

void Discord::setState(char* state) {
	discord::Activity activity{};
	activity.SetState(state);
	activity.SetDetails(currentActivity.GetDetails());
	activity.GetAssets().SetLargeImage(currentActivity.GetAssets().GetLargeImage());
	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {
	});
	currentActivity = activity;
}

void Discord::setDetails(char* details) {
	discord::Activity activity{};
	activity.SetState(currentActivity.GetState());
	activity.SetDetails(details);
	activity.GetAssets().SetLargeImage(currentActivity.GetAssets().GetLargeImage());
	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {
	});
	currentActivity = activity;
}

void Discord::setLargeImage(char* image) {
	discord::Activity activity{};
	activity.SetState(currentActivity.GetState());
	activity.SetDetails(currentActivity.GetDetails());
	activity.GetAssets().SetLargeImage(image);
	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {
	});
	currentActivity = activity;
}

void Discord::runCallbacks() {
	core->RunCallbacks();
}
#endif

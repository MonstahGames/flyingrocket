#include "MeteorSpawner.h"

USING_NS_CC;

MeteorSpawner* MeteorSpawner::setup(cocos2d::Scene* scene) {

	MeteorSpawner* spawner = new MeteorSpawner();
	spawner->setupPoints();

	scene->addChild(spawner);

	return spawner;
}

void MeteorSpawner::setMeteorSize(float size) {
	for (Node* node : this->getChildren()) {
		if (node->getName() == "meteor") {
			node->getPhysicsBody()->removeAllShapes();

			auto rot = RotateBy::create(1.50f, 320);
			node->runAction(rot);

			auto scaleDown = ScaleTo::create(1.25f, 0);
			node->runAction(scaleDown);

		}
	}
	meteorSize = size;
}

void MeteorSpawner::setupPoints() {
	int channels = initialSpawnPoints + 1;

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	float d = visibleSize.width / channels;
	float h = (visibleSize.height + 250) + origin.y;

	for (int x = 1; x < channels; x++) {
		_points.push_back(Point((d * x) + origin.x, h));
	}
}

void MeteorSpawner::spawnMeteor() {

	int size = _points.size();
	int randomIndex = RandomHelper::random_int(0, size - 1);

	Point spawnPoint = _points.at(randomIndex);

	auto meteor = Meteor::create("Meteor-hd.png", meteorSize);
	meteor->setPosition(spawnPoint);
	this->addChild(meteor, 5);

}

void MeteorSpawner::update(float delta) {
	m_runningTime = m_runningTime + delta;

	if (nextTimeToSpawn <= m_runningTime) {

		spawnMeteor();
		nextTimeToSpawn = (m_runningTime + spawnDelay);
	}
}

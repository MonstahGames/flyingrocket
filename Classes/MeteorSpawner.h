#ifndef __METEOR_SPAWNER_H__
#define __METEOR_SPAWNER_H__

#include "cocos2d.h"
#include "Meteor.h"

class MeteorSpawner : public cocos2d::Node {
public:

	static MeteorSpawner* setup(cocos2d::Scene* scene);
	float spawnDelay = 0.21f;
	void setMeteorSize(float size);
	float meteorSize = 0.5f;
	void update(float delta);

private:

	void setupPoints();
	void spawnMeteor();

	const int initialSpawnPoints = 5;
	float m_runningTime = 0;
	float nextTimeToSpawn = 0;
	std::vector<cocos2d::Point> _points;
};

#endif
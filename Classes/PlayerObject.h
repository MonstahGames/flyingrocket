#ifndef __PLAYER_OBJECT_H__
#define __PLAYER_OBJECT_H__

#include "cocos2d.h"

class PlayerObject : public cocos2d::Sprite {
public:

	PlayerObject();
	~PlayerObject();

	static PlayerObject* getInstance();

	static PlayerObject* create(const std::string& file);
	void initSprite();

	bool isDead = false;

	void movePlayer(bool left, bool isDead);

private:
	float globalSpeed = 0.075f;
	static PlayerObject* dObject;

};

#endif //__PLAYER_OBJECT_H__

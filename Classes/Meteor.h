#ifndef __METEOR_OBJECT_H__
#define __METEOR_OBJECT_H__

#include "cocos2d.h"

class Meteor : public cocos2d::Sprite {
public:

	static Meteor* create(const std::string& file, float scale);
	
private:
	void initMeteor(float scale);
	void moveMeteor();
	void destroy();
};

#endif //__METEOR_OBJECT_H__

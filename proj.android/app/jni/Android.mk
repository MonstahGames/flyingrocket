LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := FlyingRocket_shared

LOCAL_MODULE_FILENAME := libFlyingRocket

LOCAL_SRC_FILES := $(LOCAL_PATH)/hellocpp/main.cpp \
                   $(LOCAL_PATH)/../../../Classes/AppDelegate.cpp \
                   $(LOCAL_PATH)/../../../Classes/Discord.cpp \
				   $(LOCAL_PATH)/../../../Classes/GameFlow.cpp \
				   $(LOCAL_PATH)/../../../Classes/ScoreManager.cpp \
				   $(LOCAL_PATH)/../../../Classes/Border.cpp \
				   $(LOCAL_PATH)/../../../Classes/Meteor.cpp \
				   $(LOCAL_PATH)/../../../Classes/MeteorSpawner.cpp \
				   $(LOCAL_PATH)/../../../Classes/PlayerObject.cpp \
				   $(LOCAL_PATH)/../../../Classes/DeathLayer.cpp \
				   $(LOCAL_PATH)/../../../Classes/GameScene.cpp \
				   $(LOCAL_PATH)/../../../Classes/LoadingScene.cpp \
				   $(LOCAL_PATH)/../../../Classes/MenuScene.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cc_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module, cocos)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
